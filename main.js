'use strict';

let accordion = document.getElementsByClassName('accordion');

for (let i = 0; i < accordion.length; i++) {
    accordion[i].children[0].addEventListener('click', function () {
        accordion[i].classList.toggle('open');
        for (let i = 0; i < accordion.length; i++) {
            if (this !== accordion[i].children[0]) {
                accordion[i].classList.remove('open');
            }
        }
    });
}

